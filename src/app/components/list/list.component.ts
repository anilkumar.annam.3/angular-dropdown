import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
})
export class ListComponent implements OnInit {
  constructor() { }

  items = [{ text: 'abcd', id: 1 }, { text: 'abcd', id: 2 }, { text: 'abcd', id: 3 }, { text: 'abcd', id: 4 },];

  ngOnInit(): void { }

  previousId: string = '0';
  previousElement: any = null;

  toggleReference(elementRefence: any) {
    this.previousId = this.previousElement ? this.previousElement.id : null;
    if (this.previousId === null) {
      elementRefence.style.maxHeight = elementRefence.scrollHeight + "px";
      this.previousElement = elementRefence;
    } else if (this.previousId !== null && this.previousId !== elementRefence.id) {
      this.previousElement.style.maxHeight = null;
      this.previousElement = elementRefence;
      this.previousElement.style.maxHeight = this.previousElement.scrollHeight + "px";
    } else if (this.previousId === elementRefence.id) {
      elementRefence.style.maxHeight = null;
      this.previousElement = null;
    }
  }
}
