import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.css'],
})
export class DropdownComponent implements OnInit {

  constructor() { }

  ngOnInit(): void { }

  @Input() toggleElement(currentElement: any): void { }
  @Input() props: any = {};
  @ViewChild('details') detailsDiv: any;
  @Output() newItemEvent = new EventEmitter<any>();

  toggle() {
    this.newItemEvent.emit(this.detailsDiv.nativeElement);
  }
}
